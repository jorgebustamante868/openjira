import { FC } from 'react'
import Head from "next/head"

import { Box } from "@mui/system"
import { NavBar, Sidebar } from '../ui';

interface Props {
    title?:string;
}
interface Props {
    children: React.ReactNode;
}


export const Layout:FC<Props> = ({ title = 'Open Jira' , children }) => {
    return (
        <Box sx={{ flexFlow: 1}} >
            <Head>
                <title>{title}</title>
            </Head>

            {/* NavBar*/}
            <NavBar />

            {/* Sidebar */}

            <Sidebar />

            <Box sx={{ padding:'10px 20px'}}>
                { children}
            </Box>
        </Box>
    )

}