import { AppBar, IconButton, Toolbar, Typography } from "@mui/material"
import WidgetsTwoToneIcon from '@mui/icons-material/WidgetsTwoTone';
import MenuIcon from '@mui/icons-material/Menu';
import { useContext } from "react";
import { UIContext } from "../../context/ui";


export const NavBar = () => {

    const  {openSideMenu} = useContext( UIContext)

    return (
        <AppBar position="sticky" elevation={ 7 }>
            <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    onClick={ openSideMenu}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography variant='h5'>OpenJira</Typography>
            </Toolbar>
        </AppBar>
    )
}
