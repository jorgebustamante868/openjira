import { Card, CardActionArea, CardActions, CardContent, List, Paper, Typography } from "@mui/material"
import { DragEvent, FC } from "react";
import { Entry } from "../../interfaces"

interface Props { 
    entry: Entry;
}

    

export const EntryCard:FC<Props> = ( {entry} ) => {
    
    const onDragStart = ( event : DragEvent) => {
        console.log(event)

        event.dataTransfer.setData('text' , entry._id);
    }

    const onDragEnd = () => {
        
    }

    return (
        //TODO aqui haremos drop
    <Card 
        sx = {{ marginBottom: 1}}
        draggable = { true }
        onDragStart = { onDragStart }
    >
        <CardActionArea>
            <CardContent>
                <Typography sx={{ whiteSpace:'pre-line'}}> {entry.description}  </Typography>
            </CardContent>

            <CardActions sx = {{ display:'flex' , justifyContent:'end' , paddingRight:2}} >
                <Typography variant='body2'>Hace 30 min</Typography>
            </CardActions>
        </CardActionArea>

    </Card>
    
    
    )
}