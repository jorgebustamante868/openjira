import { useContext } from "react";
import { UIContext } from "../../context/ui";
import { Drawer , Box, Typography, List, ListItemIcon ,ListItem, ListItemText, Divider } from "@mui/material";

import MailOutlineIcon from '@mui/icons-material/MailOutline';
import AndroidIcon from '@mui/icons-material/Android';
const menuItems: string[] = ['Inbox','Starred' , 'Send Email' , 'Register']

export const Sidebar = () =>{
    const { sidemenuOpen , closeSideMenu } = useContext( UIContext)

    return (
        <Drawer
            anchor="left"
            open ={sidemenuOpen}
            onClose= { closeSideMenu }
            >
            <Box sx = {{ width: 180}}>

                <Box sx={{ padding:'5px 10px'}} >
                    <Typography variant="h5">Menú</Typography>
                </Box>



                <Divider/>
                <List>
                    {
                        menuItems.map( (text,index ) =>(
                            <ListItem button key = { text }>
                                <ListItemIcon>
                                    { index % 2 ? <MailOutlineIcon/> : <AndroidIcon/>  }
                                </ListItemIcon>
                                <ListItemText primary={ text } />

                            </ListItem>
                        ))
                    }

                </List>
            </Box>



        </Drawer>
    );
};