import { DragEvent, FC, useContext, useMemo } from "react"
import { List, Paper } from "@mui/material"
import { EntryCard } from "./"

import { EntriesContext } from '../../context/entries'
import { EntryStatus } from '../../interfaces'

interface Props {
    status : EntryStatus
}

export const EntryList:FC<Props> = ( { status} ) => {

    const { entries }  = useContext( EntriesContext)

    const entriesByStatus = useMemo( () =>  entries.filter( entry => entry.status === status)  , [ entries ])
    
    const allowDrop = (event :DragEvent) => {
        event.preventDefault();
    }

    const onDropEntry = ( event : DragEvent<HTMLDivElement> ) => {
        const id = event.dataTransfer.getData('text');
        console.log({id})
        // onDropEntry
    }

    return (
        //TODO aqui haremos drop
        <div 
            onDrop = { onDropEntry }
            onDragOver = { allowDrop }
        >
            <Paper sx = {{ height:'calc(100vh - 250px)' , overflow:'scroll' , backgroundColor: 'transparent' , padding: 1  }}>

                {/* // Tod Cambiara si estoy haciendo drag */}
                <List sx = {{ opacity: 1 }}>
                    {
                        entriesByStatus.map( entry =>(
                            
                            <EntryCard key={ entry._id } entry = { entry}/>
                        ))
                    }
                    


                </List>

            </Paper>
        </div>
    )
}
