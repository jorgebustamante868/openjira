import { Box, Button, TextField } from "@mui/material"
import { ChangeEvent, useContext, useState } from "react";
import SaveIcon from '@mui/icons-material/Save';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { EntriesContext } from "../../context/entries";
import { UIContext } from "../../context/ui";


export const NewEntry = () => {

    const { addNewEntry } = useContext( EntriesContext )

    const { isAddingEntry , setIsAddingEntry } = useContext( UIContext )
    // const [ isAdding , setIsAdding ] = useState(false); 

    const [ inputValue , setInputValue] = useState('');

    const [ touched , setTouched  ] = useState(false);

    const onTextFieldChanged = (event :  ChangeEvent<HTMLInputElement> ) => {

        setInputValue (event.target.value);

    }


    const onSave = () => {

        if( inputValue.length === 0 ) return 0;

        
        console.log( {inputValue} )
        addNewEntry(inputValue)
    }

    return (
        <Box sx={{ marginBottom:2, paddingX: 1 }} >
            {
                isAddingEntry ? (
                    <>
                    <TextField
                        fullWidth
                        sx={{ marginTop : 2 , marginBottom : 1}}
                        placeholder='Nueva entrada'
                        autoFocus
                        multiline

                        label = 'Nueva entrada'
                        helperText={ inputValue.length <= 0 && touched && 'Ingrese un valor' }
                        error={ inputValue.length <= 0 && touched }
                        value = { inputValue }
                        onChange= { onTextFieldChanged}
                        onBlur= { () => setTouched(true ) }
                        />

                        <Box display = 'flex'  justifyContent = 'space-between' >

                        <Button
                        variant = 'outlined'
                        color = 'secondary'
                        endIcon = { <DeleteForeverIcon/> }
                        onClick={ () => setIsAddingEntry (false) }
                        >
                            Cancelar
                        </Button>
                        <Button
                        variant = 'outlined'
                        color = 'secondary'
                        endIcon = { <SaveIcon/> }
                        onClick = { onSave }
                        >
                            Guardar
                        </Button>
                                
            
                        </Box>
                    </>
                ) : (

                    <>
                        <Button
                        startIcon={<AddCircleOutlineIcon/>} 
                        fullWidth
                        variant='outlined'

                        onClick={ () => setIsAddingEntry (true) }
                        >AGREGAR TAREA</Button>
                    </>

                )
            }
        </Box>
    
    )
}



