import { Typography } from '@mui/material'
import type { NextPage } from 'next'
import { useState } from 'react'
import { setFlagsFromString } from 'v8'

import { Layout } from '../components/layouts'

const porta: NextPage = () => {

    const [form , setForm ]= useState({
        input:"hola agetics",
    })
    const handleChange = async ( e:any )=> {

        const { name , value } =e.target;
        await setForm({
                ...form,
                [name]:value                
            });
          
    }

    const copiar = () => {

        var aux = document.createElement("input");
        aux.setAttribute("value" , form.input);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
        alert ( "se copio al porta papeles")
    }

  return (
    <Layout>

    <div className='App'>
        <input 
        className='form-control'
        type='text'
        name='input'
        onChange={ handleChange }
        
        // value={ "token-invalid-o--valid"}
        />
        <button className='btn btn-primary' onClick={copiar} >Copiar</button>
    </div>

    </Layout>
  )
}

export default porta
