import { Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import type { NextPage } from 'next'

import { Layout } from '../components/layouts'
import { EntryList, NewEntry } from '../components/ui'

const HomePage: NextPage = () => {
  return (
    <Layout title='Home OpenJira'>

      <Grid container spacing={ 2 } >
        
        <Grid item xs = { 12 } sm = { 4 }>

          <Card sx = {{ height:'calc(100vh - 100px)' }}>

            <CardHeader title="Pendientes"></CardHeader>
            <CardContent> 
              <NewEntry/>
              <EntryList status='pending'/>
            </CardContent>
          </Card>

        </Grid>

        <Grid item xs = { 12 } sm = { 4 }>

        <Card sx = {{ height:'calc(100vh - 100px)' }}>

          <CardHeader title="En Progreso"></CardHeader>
            <CardContent> 
              <EntryList status='in-process'></EntryList>
            </CardContent>
        </Card>

        </Grid>


        <Grid item xs = { 12 } sm = { 4 }>

        <Card sx = {{ height:'calc(100vh - 100px)' }}>

          <CardHeader title="Completadas"></CardHeader>
          <CardContent> 
              <EntryList status='finished'></EntryList>
            </CardContent>
        </Card>

        </Grid>
      
      </Grid>

    <Typography color='secondary'>Hola mundo Desde agetic</Typography>
    
    </Layout>
  )
}

export default HomePage
