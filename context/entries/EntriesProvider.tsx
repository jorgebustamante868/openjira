import React, { FC, useReducer } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { NewEntry } from '../../components/ui';

import { Entry } from '../../interfaces';
import { EntriesContext, entriesReducer } from "./index";


export interface EntriesState {
    entries: Entry[];
}


const Entries_INITIAL_STATE: EntriesState = {
    entries: [
        {
            _id:uuidv4(),
            description:' Penddiente Esse aliquip velit laboris veniam mollit sunt.',
            status:'pending',
            createAt:Date.now()
        },
        {
            _id:uuidv4(),
            description:'En proceso Eiusmod id ea eu culpa reprehenderit duis adipisicing ex ad duis nostrud nostrud.',
            status:'in-process',
            createAt:Date.now()-1000000
        },
        {
            _id:uuidv4(),
            description:' Finalizado Sit commodo culpa pariatur consectetur adipisicing eiusmod minim consectetur quis est.',
            status:'finished',
            createAt:Date.now()-1000
        }
    ],
}

type Props = {
    children?:React.ReactNode
}

export const EntriesProvider:FC<Props>  = ({ children }) => {

    const [state, dispatch] = useReducer( entriesReducer , Entries_INITIAL_STATE );

    const addNewEntry = (description:string) => {

        const newEntry : Entry = {

            _id: uuidv4(),
            description,
            createAt:Date.now(),
            status:'pending'
        }

        dispatch({ type: '[Entry] Add-Entry' , payload: newEntry });
     }

    return (
        <EntriesContext.Provider value={{
            ...state,

            //Metodos
            addNewEntry

            
        }}>
            { children }
        </EntriesContext.Provider>
    )
};